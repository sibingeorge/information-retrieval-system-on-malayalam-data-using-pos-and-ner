from flask import Flask, jsonify, render_template, request
import os
app = Flask(__name__)

@app.route('/_add_numbers')
def add_numbers():
    a = request.args.get('a', 0, type=str)
    b = request.args.get('b', 0, type=str)
    try:
        print(a,b)
        os.system("python3 /home/sibin/PycharmProjects/nlp/query.py " + str(a) +" "+ str(b))
        f= open("searchresult.txt",'r')
        out= f.read()
    
        return jsonify(result=out)
    except:
        return jsonify(result="error")
@app.route('/')
def index():
    return render_template('index.html')

if __name__=='__main__':
    app.run(debug=True)