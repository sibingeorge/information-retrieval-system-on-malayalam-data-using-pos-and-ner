from whoosh.qparser import QueryParser
from whoosh import scoring
from whoosh.index import open_dir
from similary import most_similar
import sys
import os

fi=[filename for filename in os.listdir('corpus')]
#print(fi)
ix = open_dir("indexdir")
f= open("searchresult.txt",'w')
if not (len(sys.argv) == 3):
    print('This program needs two inputs: search keyword and number of result')
    print('example: python query.py "hello" 5')
    sys.exit(1)

# query_str is query string
input_str = sys.argv[1]
query_str = most_similar(input_str)
#print(query_str)
# Top 'n' documents as result
topN = int(sys.argv[2])
with ix.searcher(weighting=scoring.Frequency) as searcher:
    query = QueryParser("content", ix.schema).parse(query_str)
    results = searcher.search(query, limit=topN)
    for i in range(topN):
        a = results[i]['title']
        f.write(a[9:])
        f.write("\n")
        g = open("corpus/"+a[9:],"r")
        f.write(g.read())
        #print(results[i]['title'], str(results[i].score),results[i]['textdata'])
f.close()