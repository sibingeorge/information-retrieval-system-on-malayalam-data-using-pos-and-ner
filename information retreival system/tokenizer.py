import os
import re
directory = 'corpus'
dest = 'outtoken'
for filename in os.listdir(directory):
    print(filename)
    #os.path.join(directory,filename)
    if filename.endswith(".txt"):
        f = open("corpus/"+filename,"r")
        w=open(dest+"/"+filename,"w")
        lines = f.read()
        s = re.findall(u"[\u0D00-\u0D7F,.]+",lines)
        if len(s)!=0:
            for word in s:
                w.write(word+"\n")
