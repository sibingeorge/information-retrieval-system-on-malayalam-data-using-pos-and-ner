import nltk
import os 
DEFAULTDIR = "./nerindex"
def most_similar(word,dir=DEFAULTDIR):
    most_similar = ""
    distance = 5
    filelist = os.listdir(DEFAULTDIR)
    for file in filelist:
        with open(DEFAULTDIR +"/"+ file) as inputFile:
            data = inputFile.readlines()
            for item in data :
                word_dis = nltk.edit_distance(word,item)
                if(word_dis <= distance):
                    most_similar = item
                    distance = word_dis
    if (distance >= 4):
        return word
    else:   
        return most_similar