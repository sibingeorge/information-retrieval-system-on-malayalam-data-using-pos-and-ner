# Information Retrieval System on malayalam data using POS and NER

This project proposes an effective information retrieval system and a analyt-
ics building methodology for Malayalam petitions which comes under various
departments of Government of Kerala, which are relevant to the official in-
formation need and efficient management. By building a good methodology
for analysing the petition documents, the government can arrive in a good
decision-making process. The proposed system improves effectiveness by con-
sidering Named entities and near terms specified in the query. Though the
evaluation of the system is performed over a small corpus, the results are
promising. The proposed system is thus relevant for various natural language
processing tasks in a highly agglutinative language like Malayalam.